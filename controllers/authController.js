const bcrypt = require("bcrypt");
const users = require("../public/js/user");
const { User_game, User_game_biodata } = require("../models");
const jwt = require("jsonwebtoken");

module.exports = {
  home: (req, res) => {
    res.render("home", { user: req.session.user });
  },
  loginForm: (req, res) => {
    res.render("login");
  },
  login: async (req, res) => {
    const emailLogin = req.body.email;
    const passwordLogin = req.body.password;

    let user = users.find(
      (user) => user.email === emailLogin && user.password === passwordLogin
    );

    if (!user) {
      user = await User_game.findOne({
        where: {
          email: emailLogin,
        },
        include: { model: User_game_biodata },
      });

      if (!user) {
        return res.send({
          status: "error",
          message: "Username atau password salah!",
        });
      }

      const isMatch = await bcrypt.compare(passwordLogin, user.password);

      if (!isMatch) {
        return res.send({
          status: "error",
          message: "Username atau password salah!",
        });
      }
    }

    // Create a JWT token
    const token = jwt.sign({ email: user.email }, process.env.JWT_SECRET);

    // Save the token in the session
    req.session.token = token;
    req.session.user = user;
    req.session.loggedIn = true;
    console.log("Login Berhasil");
    res.render("home", { user: req.session.user });
  },
  registerForm: (req, res) => {
    res.render("register");
  },
  register: async (req, res) => {
    const {
      nickname,
      email,
      password,
      fullName,
      location,
      phone,
      dateOfBirth,
    } = req.body;

    try {
      // Check if email already exists in the database
      const userExists = await User_game.findOne({ where: { email } });
      if (userExists) {
        return res
          .status(400)
          .send({ status: "error", message: "Email sudah terpakai" });
      }

      // Hash the password
      const hashedPassword = await bcrypt.hash(password, 10);

      // Create a new user
      const newUser = await User_game.create({
        nickname,
        email,
        password: hashedPassword,
      });

      // Create user biodata
      await User_game_biodata.create({
        userId: newUser.id,
        fullName,
        location,
        phone,
        dateOfBirth,
      });

      console.log("Data berhasil disimpan");
      res.status(200).send({
        status: "success",
        message: "Register berhasil, Data disimpan di database",
      });
    } catch (error) {
      console.error(error);
      res.status(500).send({
        status: "error",
        message: "Terjadi kesalahan saat menyimpan data",
      });
    }
  },
  logout: (req, res) => {
    req.session.destroy((err) => {
      if (err) {
        res.send({ status: "error", message: "Logout gagal" });
      } else {
        res.redirect("/");
      }
    });
  },
};
