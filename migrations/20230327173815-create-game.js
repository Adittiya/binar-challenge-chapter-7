'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Games', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      player_one: {
        type: Sequelize.STRING
      },
      player_two: {
        type: Sequelize.STRING
      },
      player_one_choices: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      player_two_choices: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      winner: {
        type: Sequelize.STRING
      },
      times: {
        type: Sequelize.STRING
      },
      room: {
        type: Sequelize.STRING
      },
      result: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Games');
  }
};