const users = require("../public/js/user");
const {
  User_game,
  User_game_biodata,
  User_game_history,
} = require("../models");

module.exports = {
  superUser: (req, res) => {
    const usersWithoutPassword = users.map((user) => {
      return {
        nick: user.nick,
        email: user.email,
      };
    });
    res.send({ users: usersWithoutPassword });
  },

  list: (req, res) => {
    User_game.findAll({ include: User_game_biodata }).then((user_games) => {
      res.render("users", {
        user_games: user_games,
      });
    });
  },
  editForm: async (req, res) => {
    try {
      const user = await User_game.findByPk(req.params.id, {
        include: User_game_biodata,
      });
      res.render("edit", { user });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  },
  edit: async (req, res) => {
    try {
      const userId = req.params.id;
      const { nickname, email, fullName, location, phone, dateOfBirth } =
        req.body;

      // update user fields
      await User_game.update({ nickname, email }, { where: { id: userId } });

      // update user biodata fields
      await User_game_biodata.update(
        { fullName, location, phone, dateOfBirth },
        { where: { userId } }
      );

      res.redirect("/users");
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  },
  delete: async (req, res) => {
    try {
      const userId = req.params.id;
      await User_game.destroy({ where: { id: userId } });
      res.redirect("/users");
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  },
  history: function (req, res) {
    const userId = req.params.id;

    User_game.findOne({ where: { id: userId }, include: User_game_history })
      .then(function (user) {
        res.render("history", { user: user });
      })
      .catch(function (error) {
        console.log(error);
        res.status(500).send("An error occurred");
      });
  },
  addHistoryForm: function (req, res) {
    const userId = req.params.id;

    User_game.findOne({ where: { id: userId } })
      .then(function (user) {
        res.render("addhistory", { user: user });
      })
      .catch(function (error) {
        console.log(error);
        res.status(500).send("An error occurred");
      });
  },
  addHistory: function (req, res) {
    const userId = req.params.id;
    const { gameName, result } = req.body;

    User_game.findOne({ where: { id: userId } })
      .then(function (user) {
        return user.createUser_game_history({ gameName, result });
      })
      .then(function () {
        res.redirect("/users/history/" + userId);
      })
      .catch(function (error) {
        console.log(error);
        res.status(500).send("An error occurred");
      });
  },
  deleteHistory: function (req, res) {
    const userId = req.params.userId;
    const gameId = req.params.gameId;

    User_game_history.destroy({ where: { id: gameId } })
      .then(function () {
        res.redirect("/users/history/" + userId);
      })
      .catch(function (error) {
        console.log(error);
        res.status(500).send("An error occurred");
      });
  },
};
