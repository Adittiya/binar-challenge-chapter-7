# Binar Academy Full-Stack Web Developer Challange 7

#### Preview

![Project](/public/images/preview.gif)

## Installation

You need to install Node.js, NPM, PostgreSql And Database Client (pgadmin4, dbeaver)

How To Use

- Clone or download Repository
- Open Directory using hyper, ubuntu, git bash, or terminal
- Install package with Run `npm i`
- Run server with `npm start` or `npm run dev`
- Open `http://localhost:3000` and other route in your browser
- login as User static admin or register as new User and will be saved on Database Postgres
- User static email: admin@gmail.com, Password: admin

## Package Used

- Express.js
- body-parser
- fs
- ejs
- express-session
- sequelize
- pg
- bcrypt
- jwt
