const jwt = require("jsonwebtoken");

function verifyToken(req, res, next) {
  const token = req.session.token;

  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: "Unauthorized" });
    }
    // Check if the decoded email matches the user's email in the session
    if (decoded.email !== req.session.user.email) {
      return res.status(401).json({ message: "Unauthorized" });
    }
    req.user = decoded.email;
    next();
  });
}

module.exports = { verifyToken };
