const { where } = require("sequelize");
const { Game, User_game } = require("../models");

async function getAllRoomsController(req, res) {
  try {
    const rooms = await Game.findAll();
    res.json(rooms);
  } catch (error) {
    console.log(error);
    res.json([]);
  }
}
async function getRoomByIdController(req, res) {
  const roomId = Number(req.params.roomId);
  try {
    const room = await Game.findOne({ where: { id: roomId } });
    res.json(room);
  } catch (error) {
    console.log(error);
    res.json(404);
  }
}

async function createRoomController(req, res) {
  try {
    if (req.body.email === null || req.body.email === "") {
      return res.status(400).json({ message: "email cannot be empty!" });
    }
    const user = await User_game.findOne({ where: { email: req.body.email } });
    const game = await Game.create({ player_one: user.email });
    res.status(200).json({ message: "Room created successfully", id: game.id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error });
  }
}

async function joinRoomController(req, res) {
  try {
    if (req.body.email === null || req.body.email === "") {
      return res.status(400).json({ message: "email cannot be empty!" });
    }
    if (req.params.roomId === null || req.params.roomId === "") {
      return res.status(400).json({ message: "roomId cannot be empty!" });
    }
    if (isNaN(Number(req.params.roomId))) {
      return res
        .status(400)
        .json({ message: "invalid roomId, expected number not a character" });
    }

    const email = req.body.email;
    const roomId = Number(req.params.roomId);
    const user = await User_game.findOne({ where: { email } });

    if (user === null) {
      return res.status(400).json({ message: "email not found" });
    }

    const room = await Game.findOne({ where: { id: roomId } });

    if (room === null) {
      return res.status(400).json({ message: "room not found" });
    }

    if (room.player_one === user.email || room.player_two === user.email) {
      return res.status(400).json({ message: " user already in the room" });
    }

    if (room.player_two != null) {
      return res.status(400).json({ message: "room is full" });
    }

    await Game.update({ player_two: user.email }, { where: { id: roomId } });

    const updatedRoom = await Game.findByPk(roomId);

    res.status(200).json({
      message: "player 2 berhasil join room",
      room: updatedRoom,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error });
  }
}

async function playGameController(req, res) {
  try {
    if (!req.body.email) {
      return res.status(400).json({ message: "Email cannot be empty!" });
    }
    if (!req.params.roomId) {
      return res.status(400).json({ message: "Room ID cannot be empty!" });
    }
    if (isNaN(Number(req.params.roomId))) {
      return res
        .status(400)
        .json({ message: "Invalid Room ID! Expected number not a character." });
    }
    if (
      !req.body.choice ||
      !["R", "P", "S"].includes(req.body.choice.toUpperCase())
    ) {
      return res.status(400).json({ message: "Invalid choice!" });
    }

    const email = req.body.email;
    const choice = req.body.choice.toUpperCase();
    const roomId = Number(req.params.roomId);
    const room = await Game.findOne({ where: { id: roomId } });

    if (!room) {
      return res.status(404).json({ message: "Room not found!" });
    }

    const playerOneChoices = room.player_one_choices || [];
    const playerTwoChoices = room.player_two_choices || [];

    if (playerOneChoices.length === 3 && playerTwoChoices.length === 3) {
      return res
        .status(400)
        .json({ message: "Game is finished! Please check the result." });
    }

    let currentTurn = null;
    if (room.player_one === email) {
      currentTurn = "PLAYER_ONE";
    } else if (room.player_two === email) {
      currentTurn = "PLAYER_TWO";
    } else {
      return res
        .status(400)
        .json({
          message: "The email is not included as the player in the room!",
        });
    }

    if (currentTurn === "PLAYER_ONE") {
      if (playerOneChoices.length > playerTwoChoices.length) {
        return res
          .status(400)
          .json({ message: "Please wait your turn, Player 1!" });
      }
      const updatedGame = await Game.update(
        { player_one_choices: [...playerOneChoices, choice] },
        { where: { id: roomId } }
      );
      return res
        .status(200)
        .json({ message: "Game updated", game: updatedGame });
    } else {
      if (
        playerOneChoices.length === 0 ||
        playerOneChoices.length === playerTwoChoices.length
      ) {
        return res
          .status(400)
          .json({ message: "Please wait your turn, Player 2!" });
      }
      const updatedGame = await Game.update(
        { player_two_choices: [...playerTwoChoices, choice] },
        { where: { id: roomId } }
      );
      return res
        .status(200)
        .json({ message: "Game updated", game: updatedGame });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: error.message });
  }
}

module.exports = {
  createRoomController,
  joinRoomController,
  getAllRoomsController,
  getRoomByIdController,
  playGameController,
};
