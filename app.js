"use strict";
require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const fs = require("fs");
const session = require("express-session");
const users = require("./public/js/user.js");
const { User_game, User_game_biodata, User_game_history } = require("./models");

const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(
  session({
    secret: "secret-key",
    resave: false,
    saveUninitialized: false,
  })
);

const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/user");
const gameRoutes = require("./routes/game");
const {
  createRoomController,
  joinRoomController,
  getAllRoomsController,
  getRoomByIdController,
  playGameController,
} = require("./controllers/roomController.js");

app.use("/", authRoutes);
app.use("/users", userRoutes);
app.use("/play", gameRoutes);

// api game rps using postman
app.get("/api/room", getAllRoomsController);
app.get("/api/room/:roomId", getRoomByIdController);
app.post("/api/room/create", createRoomController);
app.post("/api/room/:roomId/join", joinRoomController);
app.post("/api/room/:roomId/play", playGameController);

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
