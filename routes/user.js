const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const { verifyToken } = require("../middleware/verifyToken");

router.get("/", verifyToken, userController.list);
router.get("/superuser", userController.superUser);
router.get("/edit/:id", userController.editForm);
router.post("/edit/:id", userController.edit);
router.get("/delete/:id", userController.delete);
router.get("/history/:id", userController.history);
router.get("/addhistory/:id", userController.addHistoryForm);
router.post("/addhistory/:id", userController.addHistory);
router.get("/history/delete/:userId/:gameId", userController.deleteHistory);

module.exports = router;
