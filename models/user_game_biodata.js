"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game_biodata.belongsTo(models.User_game, {
        foreignKey: "userId",
        onDelete: "CASCADE",
      });
    }
  }
  User_game_biodata.init(
    {
      userId: DataTypes.INTEGER,
      status: DataTypes.BOOLEAN,
      fullName: DataTypes.STRING,
      location: DataTypes.STRING,
      phone: DataTypes.STRING,
      dateOfBirth: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "User_game_biodata",
    }
  );
  return User_game_biodata;
};
