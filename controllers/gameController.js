module.exports = {
  play: (req, res) => {
    if (req.session.loggedIn) {
      res.render("play", { user: req.session.user });
    } else {
      res.redirect("/login");
    }
  },
};
